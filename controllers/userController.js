const User = require("../models/User");
const auth = require("../auth"); 
const bcrypt = require("bcryptjs");
let salt = bcrypt.genSaltSync(10);

module.exports.registerUser = (body) => {
	return User.find({email : body.email}).then(result => {
		if (result.length > 0){
			return false; 
		} else {
			let newUser = new User({
				firstName: body.firstName,
                lastName: body.lastName,
                address: body.address,
                birthday: body.birthday,
                gender: body.gender,
                contactNo: body.contactNo,
                shirtSize: body.shirtSize,
				email : body.email,
				password : bcrypt.hashSync(body.password, salt)
			});
		
			return newUser.save().then((user, error) => {
				if (error){
					return false; 
				} else {
					return true; 
				}
			})
		}
	})
}

module.exports.getAllUsers = () => {
	return User.find().then(result => {
		return result;
	})
}


module.exports.loginUser = (body) => {
	return User.findOne({email : body.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password);

			if(isPasswordCorrect){
				return {access : auth.createAccessToken(result.toObject())}
			} else {
				return false; 
			}
		}
	})
}

module.exports.setAsAdmin = (userId) => {
	return User.findById(userId).then(user => {
		if(user === null){
			return false;
		}else{
			user.isAdmin = true;
			return user.save().then((updatedUser, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})
		}
	})
}



module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = undefined;
		return result;
	})
}