const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	address: {
		type: String,
		required: [true, "Address is required"]
	},
	birthday: {
		type: Date,
		required: [true, "Birthday is required"]
	},
	gender: {
		type: String,
		required: [true, "Gender is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	contactNo: {
		type: String,
		required: [true, "Mobile No. is required"]
	},
	shirtSize: {
		type: String,
		required: [true, "Shirt size is required"]
	},
	dateRegistered : {
				type : Date,
				default : new Date()
	},
	isAdmin : { 
		type : Boolean,
		default : false 
	}
})

module.exports = mongoose.model("User", userSchema);
//module.exports allows us to use the file as a module, similar to packages, and can be used by other files